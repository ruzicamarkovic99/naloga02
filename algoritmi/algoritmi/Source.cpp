// A C++ program for Dijkstra's single source shortest path algorithm. 
// The program is for adjacency matrix representation of the graph 

#include <limits.h> 
#include <stdio.h> 
#include<iostream>
#include <cstring>
#include <cstdlib>
using namespace std;
#define MAX_MATCHES 100


// Number of vertices in the graph 
#define V 9 

// A utility function to find the vertex with minimum distance value, from 
// the set of vertices not yet included in shortest path tree 
int minDistance(int dist[], bool sptSet[])
{
	// Initialize min value 
	int min = INT_MAX, min_index;

	for (int v = 0; v < V; v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

// A utility function to print the constructed distance array 
void printSolution(int dist[])
{
	printf("Vertex \t\t Distance from Source\n");
	for (int i = 0; i < V; i++)
		printf("%d \t\t %d\n", i, dist[i]);
}

// Function that implements Dijkstra's single source shortest path algorithm 
// for a graph represented using adjacency matrix representation 
void dijkstra(int graph[V][V], int src)
{
	int dist[V]; // The output array. dist[i] will hold the shortest 
	// distance from src to i 

	bool sptSet[V]; // sptSet[i] will be true if vertex i is included in shortest 
	// path tree or shortest distance from src to i is finalized 

	// Initialize all distances as INFINITE and stpSet[] as false 
	for (int i = 0; i < V; i++)
		dist[i] = INT_MAX, sptSet[i] = false;

	// Distance of source vertex from itself is always 0 
	dist[src] = 0;

	// Find shortest path for all vertices 
	for (int count = 0; count < V - 1; count++) {
		// Pick the minimum distance vertex from the set of vertices not 
		// yet processed. u is always equal to src in the first iteration. 
		int u = minDistance(dist, sptSet);

		// Mark the picked vertex as processed 
		sptSet[u] = true;

		// Update dist value of the adjacent vertices of the picked vertex. 
		for (int v = 0; v < V; v++)

			// Update dist[v] only if is not in sptSet, there is an edge from 
			// u to v, and total weight of path from src to v through u is 
			// smaller than current value of dist[v] 
			if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
				&& dist[u] + graph[u][v] < dist[v])
				dist[v] = dist[u] + graph[u][v];
	}

	// print the constructed distance array 
	printSolution(dist);
}
void merge(int* a, int s, int e) {
	int mid = (s + e) / 2;

	int i = s;
	int j = mid + 1;
	int k = s;

	int temp[100];

	while (i <= mid && j <= e) {
		if (a[i] < a[j]) {
			temp[k++] = a[i++];
		}
		else {
			temp[k++] = a[j++];
		}
	}
	while (i <= mid) {
		temp[k++] = a[i++];
	}
	while (j <= e) {
		temp[k++] = a[j++];
	}

	//We need to copy all element to original arrays
	for (int i = s; i <= e; i++) {
		a[i] = temp[i];
	}


}

void mergeSort(int a[], int s, int e) {
	//Base case - 1 or 0 elements
	if (s >= e) {
		return;
	}

	//Follow 3 steps
	//1. Divide
	int mid = (s + e) / 2;

	//Recursively the arrays - s,mid and mid+1,e
	mergeSort(a, s, mid);
	mergeSort(a, mid + 1, e);

	//Merge the two parts
	merge(a, s, e);

}


//Array to store matched indexes
int FOUND[MAX_MATCHES];
//variable to store last index in FOUND array
static int l = 0;

//Partial match table
void kmp_table(string W, int* T)
{
	int pos = 2;
	int cnd = 0;
	int length = W.length();

	T[0] = -1;
	T[1] = 0;

	while (pos < length)
	{
		if (W[pos - 1] == W[cnd])
		{
			T[pos] = cnd + 1;
			cnd++;
			pos++;
		}
		else if (cnd > 0)
			cnd = T[cnd];
		else
		{
			T[pos] = 0;
			pos++;
		}
	}
}

//Search function
void kmp_search(string S, string W)
{

	int m = 0;
	int i = 0;
	int sizeS = S.length();
	int sizeW = W.length();

	int* T = new int[sizeof(int) * sizeW];

	kmp_table(W, T);

	while ((m + i) < sizeS)
	{
		if (W[i] == S[m + i])
		{
			if (i == (sizeW - 1))
			{
				//Add the start index of match in the FOUND table
				FOUND[l++] = m;
			}

			i++;
		}
		else
		{
			if (T[i] > -1)
			{
				m = m + i - T[i];
				i = T[i];
			}
			else
			{
				m = m + 1;
				i = 0;
			}
		}
	}

	delete(T);
}



// driver program to test above function 
int main()
{
	/* Let us create the example graph discussed above */
	int graph[V][V] = { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
						{ 4, 0, 8, 0, 0, 0, 0, 11, 0 },
						{ 0, 8, 0, 7, 0, 4, 0, 0, 2 },
						{ 0, 0, 7, 0, 9, 14, 0, 0, 0 },
						{ 0, 0, 0, 9, 0, 10, 0, 0, 0 },
						{ 0, 0, 4, 14, 10, 0, 2, 0, 0 },
						{ 0, 0, 0, 0, 0, 2, 0, 1, 6 },
						{ 8, 11, 0, 0, 0, 0, 1, 0, 7 },
						{ 0, 0, 2, 0, 0, 0, 6, 7, 0 } };

	dijkstra(graph, 0);
	cout << "**************************" << endl;
	int a[100];
	int n;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	mergeSort(a, 0, n - 1);

	for (int i = 0; i < n; i++) {
		cout << a[i] << " , ";
	}
	cout << endl;
	cout << "**************************" << endl;

	string S = "ABCDBCAAB ABCDABCDABDE ABCDABD";
	string W = "ABCD";

	kmp_search(S, W);

	for (int i = 0; i < l; i++)
		cout << "Pattern found at " << FOUND[i] << endl;

	return 0;
}
